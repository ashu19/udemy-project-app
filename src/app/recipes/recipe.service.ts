import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

import { Recipe } from "./recipe.model";
import { Ingredient } from "../shared/ingredient.model";
import { ShoppingListService } from "../shopping-list/shopping-list.service";

@Injectable()
export class RecipeService {
    recipesChanged = new Subject<Recipe[]>();

    private recipes: Recipe[] = [
        new Recipe('Tasty Apple Pie',
            'Perfect sweet apple pie dessert',
            'https://c1.staticflickr.com/4/3463/3962365746_e642021674_b.jpg',
            [
                new Ingredient('Apple', 4),
                new Ingredient('Flour', 1),
                new Ingredient('Sugar', 5)
            ]),
        new Recipe('Classic Chicken Burger',
            'Classic burger with roasted chicken and cheese',
            'https://pixnio.com/free-images/2017/06/08/2017-06-08-08-33-50-900x600.jpg',
            [
                new Ingredient('Bun', 2),
                new Ingredient('Chicken', 1),
                new Ingredient('Onion', 1),
                new Ingredient('Cheese slice', 2)
            ])
    ];

    constructor(private slService: ShoppingListService) {}

    setRecipes(recipes: Recipe[]) {
        this.recipes = recipes;
        this.recipesChanged.next(this.recipes.slice());
    }

    getRecipes() {
        return this.recipes.slice();
    }

    getRecipe(index: number) {
        return this.recipes[index];
    }

    addIngredientsToShoppingList(ingredients: Ingredient[]) {
        this.slService.addIngredients(ingredients);
    }

    addRecipe(recipe: Recipe) {
        this.recipes.push(recipe);
        this.recipesChanged.next(this.recipes.slice());
    }

    updateRecipe(index: number, newRecipe: Recipe) {
        this.recipes[index] = newRecipe;
        this.recipesChanged.next(this.recipes.slice());
    }

    deleteRecipe(index: number) {
        this.recipes.splice(index, 1);
        this.recipesChanged.next(this.recipes.slice());
    }
}